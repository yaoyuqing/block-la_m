module.exports = {
  plugins: [
    require('autoprefixer')({
      browsers: [
        'ie >= 11',
        'ff >= 30',
        'chrome >= 34',
        'safari >= 7',
        'opera >= 23',
        'ios >= 7',
        'android >= 4.4',
        'bb >= 10'
      ]
    }),
    require('postcss-px2vw')({
      viewportWidth: 750, // 对应设计图的宽度，用于计算 vw。默认 750，指定 0 或 false 时禁用
      rootValue: 75, // 根字体大小，用于计算 rem。默认 75，指定 0 或 false 时禁用
      unitPrecision: 5, // 计算结果的精度，默认 5
      minPixelValue: 1 // 小于等于该值的 px 单位不作处理，默认 1
    })
  ]
};
