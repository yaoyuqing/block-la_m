/*
 * @Description: webpack.dev.config.js
 * @Author: yao yu qing
 * @Date: 2020-11-09 19:08:34
 * @LastEditTime: 2021-03-12 18:25:34
 * @LastEditors: yyq
 * @FilePath: \gulp4-webpack4-demo\build\webpack.dev.config.js
 */
// const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path');
const webpack = require('webpack');
const config = require('../config/dev-server-config');
module.exports = {
  devtool: 'cheap-module-eval-source-map',
  devServer: config,
  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ]
};
