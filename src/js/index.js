/*
 * @Description:
 * @Author: yao yu qing
 * @Date: 2020-11-09 19:08:34
 * @LastEditTime: 2021-03-31 14:10:49
 * @LastEditors: yyq
 * @FilePath: \Block-la\src\js\index.js
 */

// main
import '../css/main.scss';
import './main';
window.$ = $;
