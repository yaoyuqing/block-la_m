/*
 * @Description: main.js
 * @Author: yao yu qing
 * @Date: 2021-03-17 11:40:32
 * @LastEditTime: 2021-04-20 13:49:01
 * @LastEditors: yyq
 * @FilePath: \block-la_m\src\js\main.js
 */

// vender.js
import Swiper from 'swiper';
import 'jquery-mousewheel';
import anime from 'animejs';
import { WOW } from 'wowjs';

$(function () {
  if ($('.bodyindex')[0]) {
    let indexPage = new IndexPage();
    indexPage.init();
  } else if ($('.bodyproductpost')[0]) {
    let projectPostPage = new ProjectPostPage();
    projectPostPage.init();
  }
});

// 首页
class IndexPage {}

// 产品列表页
class ProjectPostPage {}

// wowinit
function initWow() {
  window.wow = new WOW();
  window.wow.init();
}

// wow repeat
function wowHidden(index) {
  $('.wow').each(function () {
    if ($(this).closest($('.module')).index() === index) {
      var animationName = $(this).data('animation-name') || 'fadeInUp';
      $(this)
        .css({
          visibility: 'visible',
          'animation-name': animationName
        })
        .addClass('animated');
    } else {
      $(this)
        .css({
          visibility: 'hidden',
          'animation-name': 'none'
        })
        .removeClass('animated');
    }
  });
}

// jq.pin
function pin(options, $dom) {
  var scrollY = 0;
  var elements = [];
  var disabled = false;
  var $window = $(window);

  options = $.extend(
    {
      distance: {
        bottom: 0
      }
    },
    options
  );

  var recalculateLimits = function () {
    for (var i = 0, len = elements.length; i < len; i++) {
      var $this = elements[i];

      if (options.minWidth && $window.width() <= options.minWidth) {
        if ($this.parent().is('.pin-wrapper')) {
          $this.unwrap();
        }
        $this.css({
          width: '',
          left: '',
          top: '',
          position: ''
        });
        if (options.activeClass) {
          $this.removeClass(options.activeClass);
        }
        disabled = true;
        continue;
      } else {
        disabled = false;
      }

      var $container = options.containerSelector
        ? $this.closest(options.containerSelector)
        : $(document.body);
      var offset = $this.offset();
      var containerOffset = $container.offset();
      var parentOffset = $this.offsetParent().offset();

      if (!$this.parent().is('.pin-wrapper')) {
        $this.wrap('<div class="pin-wrapper"></div>');
      }

      var pad = $.extend(
        {
          top: 0,
          bottom: 0
        },
        options.padding || {}
      );

      $this.data('pin', {
        pad: pad,
        from:
          (options.containerSelector ? containerOffset.top : offset.top) -
          pad.top,
        to:
          containerOffset.top +
          $container.height() -
          $this.outerHeight() -
          pad.bottom,
        end: containerOffset.top + $container.height(),
        parentTop: parentOffset.top
      });

      $this.removeAttr('style').css({ width: $this.outerWidth() });
      $this.parent().removeAttr('style').css('height', $this.outerHeight());
    }
  };

  var onScroll = function () {
    if (disabled) {
      return;
    }

    scrollY = $window.scrollTop();

    var elmts = [];
    for (var i = 0, len = elements.length; i < len; i++) {
      var $this = $(elements[i]);
      var data = $this.data('pin');

      if (!data) {
        // Removed element
        continue;
      }

      elmts.push($this);

      var from = data.from;
      var to = data.to - options.distance.bottom;

      if (from + $this.outerHeight() > data.end) {
        $this.css('position', '');
        continue;
      }

      if (from < scrollY && to > scrollY) {
        !($this.css('position') === 'fixed') &&
          $this
            .css({
              left: '',
              top: data.pad.top
            })
            .css('position', 'fixed');
        if (options.activeClass) {
          $this.addClass(options.activeClass);
        }
      } else if (scrollY >= to) {
        $this
          .css({
            left: '',
            top: to - data.parentTop
          })
          .css('position', 'absolute');
        if (options.activeClass) {
          $this.addClass(options.activeClass);
        }
      } else {
        $this.css({ position: '', top: '', left: '' });
        if (options.activeClass) {
          $this.removeClass(options.activeClass);
        }
      }
    }
    elements = elmts;
  };

  var update = function () {
    recalculateLimits();
    onScroll();
  };

  $dom.each(function (index, item) {
    var $item = $(item);
    var data = $item.data('pin') || {};

    if (data && data.update) {
      return;
    }
    elements.push($item);
    $('img', $item).one('load', recalculateLimits);
    data.update = update;
    $item.data('pin', data);
  });

  $window.on('scroll', onScroll);
  $window.on('resize', function () {
    recalculateLimits();
  });
  recalculateLimits();

  $window.on('load', update);

  return update;
}
