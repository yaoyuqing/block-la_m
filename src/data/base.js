/*
 * @Description: base data
 * @Author: yao yu qing
 * @Date: 2021-04-06 13:18:49
 * @LastEditTime: 2021-04-06 14:40:25
 * @LastEditors: yyq
 * @FilePath: \Block-la\src\data\base.js
 */
const BASE_URL = process.env.NODE_ENV === 'development' ? '/' : './';

const BASE_DATA = {
  BASE_URL,
  headTag: {
    title: 'Block-la'
  },
  base: {
    header: {
      logo: BASE_URL + 'assets/images/logo.png',
      nav: [
        '产品',
        '解决方案',
        '客户案例',
        '获取报价',
        'Block.la动态',
        '关于我们'
      ],
      lang: [
        '简体中文',
        'English',
        'Bahasa Indonesia',
        'Espanõl',
        'Français',
        '한국어'
      ]
    }
  }
};

module.exports = BASE_DATA;
