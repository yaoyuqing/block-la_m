const merge = require('utils-merge');
const BASE = require('./base');

const INDEX_DATA = {
  bodyClass: 'bodyindex',
  indexData: [
    {
      title: '首页',
      mname: 'slider',
      content: [
        {
          title: '',
          subtitle: '',
          image: BASE.BASE_URL + 'assets/images/slider_img.png',
          url: '',
          datas: [
            '21.02',
            '20.68',
            '28.36',
            '28.40',
            '30.68',
            '21.54',
            '31.90',
            '24.57',
            '30.62',
            '21.90'
          ]
        },
        {
          title: '',
          subtitle: '',
          image: BASE.BASE_URL + 'assets/images/slider_img.jpg',
          url: '',
          datas: [
            '21.02',
            '20.68',
            '28.36',
            '28.40',
            '30.68',
            '21.54',
            '31.90',
            '24.57',
            '30.62',
            '21.90'
          ]
        }
      ]
    },
    {
      title: 'Block.la产品',
      mname: 'product',
      content: [
        {
          image: '',
          title: '现货交易系统',
          info: [
            {
              title: '高性能撮合引擎',
              desc: '内存撮合强大，每个交易对5万+tps'
            },
            {
              title: '专业交易',
              desc:
                '提供自定义和第三方两种K线，技术分析和划线工具，支持策略委托和API下单'
            },
            {
              title: '配套功能',
              desc:
                '配套财务系统，风控体系，支持所有主流币种及其代币，配套后台审核和运营CMS系统，快速稳定的系统部署和交付'
            },
            {
              title: '多种交易模式',
              desc:
                '支持法币、币币交易等多种交易形式，支持法币直接购买，且无最小限制'
            }
          ]
        },
        {
          image: '',
          title: '合约交易系统',
          info: [
            {
              title: '高性能撮合引擎',
              desc: '内存撮合强大，每个交易对5万+tps'
            },
            {
              title: '专业交易',
              desc:
                '提供自定义和第三方两种K线，技术分析和划线工具，支持策略委托和API下单'
            },
            {
              title: '配套功能',
              desc:
                '配套财务系统，风控体系，支持所有主流币种及其代币，配套后台审核和运营CMS系统，快速稳定的系统部署和交付'
            },
            {
              title: '多种交易模式',
              desc:
                '支持法币、币币交易等多种交易形式，支持法币直接购买，且无最小限制'
            }
          ]
        },
        {
          image: '',
          title: '去中⼼化钱包系统',
          info: [
            {
              title: '高性能撮合引擎',
              desc: '内存撮合强大，每个交易对5万+tps'
            },
            {
              title: '专业交易',
              desc:
                '提供自定义和第三方两种K线，技术分析和划线工具，支持策略委托和API下单'
            },
            {
              title: '配套功能',
              desc:
                '配套财务系统，风控体系，支持所有主流币种及其代币，配套后台审核和运营CMS系统，快速稳定的系统部署和交付'
            },
            {
              title: '多种交易模式',
              desc:
                '支持法币、币币交易等多种交易形式，支持法币直接购买，且无最小限制'
            }
          ]
        },
        {
          image: '',
          title: '杠杆交易系统',
          info: [
            {
              title: '高性能撮合引擎',
              desc: '内存撮合强大，每个交易对5万+tps'
            },
            {
              title: '专业交易',
              desc:
                '提供自定义和第三方两种K线，技术分析和划线工具，支持策略委托和API下单'
            },
            {
              title: '配套功能',
              desc:
                '配套财务系统，风控体系，支持所有主流币种及其代币，配套后台审核和运营CMS系统，快速稳定的系统部署和交付'
            },
            {
              title: '多种交易模式',
              desc:
                '支持法币、币币交易等多种交易形式，支持法币直接购买，且无最小限制'
            }
          ]
        },
        {
          image: '',
          title: '现货交易系统',
          info: [
            {
              title: '高性能撮合引擎',
              desc: '内存撮合强大，每个交易对5万+tps'
            },
            {
              title: '专业交易',
              desc:
                '提供自定义和第三方两种K线，技术分析和划线工具，支持策略委托和API下单'
            },
            {
              title: '配套功能',
              desc:
                '配套财务系统，风控体系，支持所有主流币种及其代币，配套后台审核和运营CMS系统，快速稳定的系统部署和交付'
            },
            {
              title: '多种交易模式',
              desc:
                '支持法币、币币交易等多种交易形式，支持法币直接购买，且无最小限制'
            }
          ]
        },
        {
          image: '',
          title: '现货交易系统',
          info: [
            {
              title: '高性能撮合引擎',
              desc: '内存撮合强大，每个交易对5万+tps'
            },
            {
              title: '专业交易',
              desc:
                '提供自定义和第三方两种K线，技术分析和划线工具，支持策略委托和API下单'
            },
            {
              title: '配套功能',
              desc:
                '配套财务系统，风控体系，支持所有主流币种及其代币，配套后台审核和运营CMS系统，快速稳定的系统部署和交付'
            },
            {
              title: '多种交易模式',
              desc:
                '支持法币、币币交易等多种交易形式，支持法币直接购买，且无最小限制'
            }
          ]
        },
        {
          image: '',
          title: '现货交易系统',
          info: [
            {
              title: '高性能撮合引擎',
              desc: '内存撮合强大，每个交易对5万+tps'
            },
            {
              title: '专业交易',
              desc:
                '提供自定义和第三方两种K线，技术分析和划线工具，支持策略委托和API下单'
            },
            {
              title: '配套功能',
              desc:
                '配套财务系统，风控体系，支持所有主流币种及其代币，配套后台审核和运营CMS系统，快速稳定的系统部署和交付'
            },
            {
              title: '多种交易模式',
              desc:
                '支持法币、币币交易等多种交易形式，支持法币直接购买，且无最小限制'
            }
          ]
        }
      ]
    },
    {
      title: '解决方案',
      mname: 'project',
      content: [
        {
          image: '',
          title: '交易系统解决方案',
          des: ['解决方案一', '解决方案二', '解决方案三']
        },
        {
          image: '',
          title: '钱包系统解决方案',
          des: ['解决方案一', '解决方案二', '解决方案三']
        },
        {
          image: '',
          title: '流动性系统解决方案',
          des: ['解决方案一', '解决方案二', '解决方案三']
        },
        {
          image: '',
          title: '金融衍生品系统解决方案',
          des: ['解决方案一', '解决方案二', '解决方案三']
        }
      ]
    },
    {
      title: '我们的案例',
      mname: 'case',
      content: [
        {
          image: BASE.BASE_URL + 'assets/images/case_img.png'
        },
        {
          image: BASE.BASE_URL + 'assets/images/case_img.png'
        },
        {
          image: BASE.BASE_URL + 'assets/images/case_img.png'
        },
        {
          image: BASE.BASE_URL + 'assets/images/case_img.png'
        },
        {
          image: BASE.BASE_URL + 'assets/images/case_img.png'
        },
        {
          image: BASE.BASE_URL + 'assets/images/case_img.png'
        }
      ]
    },
    {
      title: '安全服务',
      mname: 'service',
      content: [
        {
          image: BASE.BASE_URL + 'assets/images/service_img1.png',
          title: '智能合约及DApp安全审计服务',
          des:
            '基于形式化验证技术对多个链平台的智能合约及DAPP代码进行安全审计，确保智能合约代码实现与设计和需求的一致性，及自身的安全性，出具权威审计报告。'
        },
        {
          image: BASE.BASE_URL + 'assets/images/service_img2.png',
          title: '数字金融企业安全服务',
          des:
            '为数字金融企业提供合规、安全态势感知（威胁预警、报警和阻断）、安全顾问、渗透测试、漏洞挖掘、防御部署和威胁情报等安全服务，帮助数字金融企业构建全面的安全保障体系。'
        },
        {
          image: BASE.BASE_URL + 'assets/images/service_img3.png',
          title: '区块链应用安全服务',
          des:
            '检测区块链应用服务端和APP中的账户、数据、组件、通信模块、业务逻辑等方面存在的安全问题。提供安全加固服务，保障应用系统的安全性。'
        }
      ]
    },
    {
      title: '联系我们',
      mname: 'contact',
      content: [
        {
          title: '产品',
          link: [
            {
              name: '现货交易系统',
              link: '/'
            },
            {
              name: '合约交易系统',
              link: '/'
            },
            {
              name: '去中⼼化钱包系统',
              link: '/'
            },
            {
              name: '杠杆交易系统',
              link: '/'
            }
          ]
        },
        {
          title: '解决方案',
          link: [
            {
              name: '交易系统解决⽅案',
              link: '/'
            },
            {
              name: '流动性系统解决⽅案',
              link: '/'
            },
            {
              name: '钱包系统解决⽅案',
              link: '/'
            },
            {
              name: '⾦融衍⽣品系统解决⽅案',
              link: '/'
            }
          ]
        },
        {
          title: '工具',
          link: [
            {
              name: '客户端下载',
              link: '/'
            },
            {
              name: 'API文档',
              link: '/'
            },
            {
              name: '数字资产介绍',
              link: '/'
            },
            {
              name: '官方验证通道',
              link: '/'
            }
          ]
        }
      ]
    }
  ]
};

module.exports = merge(BASE, INDEX_DATA);
