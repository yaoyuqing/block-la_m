/*
 * @Description: project_post data
 * @Author: yao yu qing
 * @Date: 2021-04-06 13:16:28
 * @LastEditTime: 2021-04-08 17:58:25
 * @LastEditors: yyq
 * @FilePath: \Block-la\src\data\product_post.js
 */
const merge = require('utils-merge');
const BASE = require('./base');

const PROJECT_POST_DATA = {
  bodyClass: 'bodyproduct bodyproductpost',
  projectPostData: {
    postNav: [
      {
        host: 'feature',
        title: '功能特性'
      },
      {
        host: 'advantage',
        title: '产品优势'
      },
      {
        host: 'prolist',
        title: '产品清单'
      },
      {
        host: 'question',
        title: '常见问题'
      }
    ],
    feature: [
      {
        icon: BASE.BASE_URL + 'assets/images/icon1.png',
        title: '多种合约交易类型',
        subtitle:
          '合约双向开仓；交易品种自定义；支持交割和永续合约，支持逐仓和全仓模式',
        taps: [
          {
            name: '双向持仓',
            des: '灵活持仓，满足用户个性化需求'
          },
          {
            name: '多种委托',
            des: '冰山委托、加权委托、计划委托（止盈止损）'
          },
          {
            name: '全币种/永续',
            des: '不仅支持rest API，同时有公有、私有数据的差量推送'
          },
          {
            name: '正反向合约一键配置',
            des: '币本位合约、USDT合约，小币种合约'
          }
        ]
      },
      {
        icon: BASE.BASE_URL + 'assets/images/icon2.png',
        title: '专业合约量化',
        subtitle:
          '专业合约量化体系，系统自动匹配最优价格成交，快速获得最优的执行价格，极大提升成交速度',
        taps: [
          {
            name: '双向持仓',
            des: '灵活持仓，满足用户个性化需求'
          },
          {
            name: '多种委托',
            des: '冰山委托、加权委托、计划委托（止盈止损）'
          },
          {
            name: '全币种/永续',
            des: '不仅支持rest API，同时有公有、私有数据的差量推送'
          },
          {
            name: '正反向合约一键配置',
            des: '币本位合约、USDT合约，小币种合约'
          }
        ]
      },
      {
        icon: BASE.BASE_URL + 'assets/images/icon3.png',
        title: '金融级多重风控',
        subtitle:
          '通过制度⻛控、系统⻛控，以⾦融级的标准进⾏实时 监控，真正实现平台“0”亏损',
        taps: [
          {
            name: '双向持仓',
            des: '灵活持仓，满足用户个性化需求'
          },
          {
            name: '多种委托',
            des: '冰山委托、加权委托、计划委托（止盈止损）'
          },
          {
            name: '全币种/永续',
            des: '不仅支持rest API，同时有公有、私有数据的差量推送'
          },
          {
            name: '正反向合约一键配置',
            des: '币本位合约、USDT合约，小币种合约'
          }
        ]
      }
    ],
    advantage: [
      {
        title: '健全的管理后台',
        des:
          '健全的后台管理，包含⾃主上币、资产详情、报表统计等功能，便于交易所的运营⼯作',
        image: BASE.BASE_URL + 'assets/images/manage.png'
      },
      {
        title: '⾼性能撮合引擎',
        des:
          '健全的后台管理，包含⾃主上币、资产详情、报表统计等功能，便于交易所的运营⼯作',
        image: BASE.BASE_URL + 'assets/images/manage.png'
      },
      {
        title: '完备的⻛险防范制度',
        des:
          '健全的后台管理，包含⾃主上币、资产详情、报表统计等功能，便于交易所的运营⼯作',
        image: BASE.BASE_URL + 'assets/images/manage.png'
      }
    ],
    prolist: [
      {
        title: '产品功能',
        list: [
          ['双向持仓', '多元化⻛控', '永续/全币种'],
          ['多种委托', '正反向合约', '⻛险限额'],
          ['⾃定义返佣', '⼿续费', '合约倍数⾃定义']
        ]
      },
      {
        title: '产品服务',
        list: [
          ['双向持仓', '多元化⻛控', '永续/全币种'],
          ['多种委托', '正反向合约', '⻛险限额'],
          ['⾃定义返佣', '⼿续费', '合约倍数⾃定义']
        ]
      },
      {
        title: '增值服务',
        list: [
          ['双向持仓', '多元化⻛控', '永续/全币种'],
          ['多种委托', '正反向合约', '⻛险限额'],
          ['⾃定义返佣', '⼿续费', '合约倍数⾃定义']
        ]
      }
    ],
    question: [
      '最⼤杠杆是多少，客户能否选择低于最⼤杠杆的杠杆？',
      '是否⽀持通过websocket下订单和取消订单，还是严格推送数据？',
      '系统可以⽀持多少个客户帐户？'
    ]
  }
};

module.exports = merge(BASE, PROJECT_POST_DATA);
